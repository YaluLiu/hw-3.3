#ifndef LINKLIST_H
#define LINKLIST_H

typedef struct node
{
    char  pcKey[30];
    void* pvValue;
    struct node* next;
}node;

typedef node* Link_t;

// if errno = 0 func runs correct, else make error 
int   errno;
//Create a new Link
Link_t Link_new(void);


//free the memory of the Link
void Link_free(Link_t oLink);


//return the num of the node
int Link_getLength(Link_t oLink);

//print the list
void Link_print(Link_t oLink);

//insert the pckey. when exist or no memory failed. return 1 if success, else return 0.
int Link_put(Link_t oLink,
             const char *pcKey,
             const void *pvValue);

//find pckey. if found return 1, else return 0.
int Link_contains(Link_t oLink, const char *pcKey);

//replace the old  value with new value.
void *Link_replace(Link_t oLink, const char *pcKey,const void *pvValue);


//find pckey. if found return value. else return NULL pointer.
void *Link_get(Link_t oLink, const char *pcKey);

//reove the node. return the pointer.
void *Link_remove(Link_t oLink, const char *pcKey);


#endif // LINKLIST_H_INCLUDED
