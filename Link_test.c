#include <stdio.h>
#include <stdlib.h>

#include "LinkList.h"

static int success ;
static int fail    ;

void test_begin(void) 
{
    printf("begin the test----------------- \n");
}

void test_end(Link_t head) 
{
    printf("\nThe num of success is %d\n ", success);
    printf("  The num of fail    is %d\n ", fail);
    Link_free(head);
    printf("\nend the test------------------- \n");
}

void test_try(int isTrue, const char* msg) 
{
    if(isTrue) 
    {
        ++success;
        printf("%s passed the case !Success! \n",msg);
    }
    else
    {
        ++fail;
        printf("%s failed to pass the normal case\n",msg);
    }
}

static void test_Link_new_free() 
{
  Link_t s = Link_new();
  errno = 0;
  test_try(errno != -1, "The func Link_new_free()");
  Link_free(s);
  return ;
}

static void test_Link_put(Link_t head)
{
    printf("\ntest the Link_put--------------\n");
    FILE *fp;
    fp = fopen("Link_put.txt","r");
    if(fp == NULL)  
    {
        printf("file can't open!\n");
        return ;
    }
    int num = 0;char str[20];
    while(fscanf(fp,"%d",&num))
    {
        //printf("scanfing!\n");
        if(num == -1) break;
        fscanf(fp,"%s",str);
//        printf("%d %s",num,str);
        printf("insert the %dth node\n",num);
        errno = 0;
        int status = Link_put(head,str,num);
        test_try(errno != -1,"The func put()");
        // printf("bug1\n");
    }
    fclose(fp);
    Link_print(head);
    return ;
}

static void test_Link_contains(Link_t head)
{
    printf("\ntest the Link_contains---------------\n");
    errno = 0;
    int status = Link_contains(head,"nunu"); 
    test_try(errno != -1,"The func Link_contains()");
        // printf("bug1\n");
    if(status)   
        printf("nunu exist!\n");
    else          
        printf("nunu not exist!\n");
    errno = 0;
    status = Link_contains(head,"nuli");
    test_try(errno != -1,"The func Link_contains()");
        // printf("bug1\n");
    if(status)    
        printf("nuli exist!\n");
    else         
        printf("nuli not exist!\n");
    Link_print(head);
    return ;
}

static void test_Link_get(Link_t head)
{
    printf("\ntest the Link_get---------------\n");
    errno = 0;
    void* t = Link_get(head,"lili");
    Link_t target = (Link_t)t;
    test_try(errno != -1,"The func get()");
        // printf("bug1\n");
    if(target == NULL)
        printf("lili not exist in the link!\n");
    else
        printf("lili is the %dth\n",target->pvValue);
        // printf("bug1\n");
    errno = 0;
    target = Link_get(head,"xyz");
    test_try(errno != -1,"The func get()");
    if(target == NULL)
        printf("xyz not exist in the link!\n");
    else
        printf("xyz is the %dth\n",target->pvValue);
    Link_print(head);
    return ;
}

static void test_Link_getLength(Link_t head)
{
    printf("\ntest the Link_getLength---------------\n");
    errno = 0;
    int Length = Link_getLength(head);
    test_try(errno != -1,"The func getLength()");
    printf("The list's length is %d\n",Length);
    Link_print(head);
    return ;
}

static void test_Link_replace(head)
{
    printf("\ntest the Link_replace---------------\n");
    errno = 0;
    Link_t target = Link_replace(head,"nunu",10);
    test_try(errno != -1,"The func replace()");
    if(target == NULL)
        printf("nunu not exist in the link!\n");
    else
        printf("nunu is the %dth\n",target->pvValue);
    errno = 0;
    target = Link_replace(head,"xyz",10);
    test_try(errno != -1,"The func replace()");
    if(target == NULL)
        printf("xyz not exist in the link!\n");
    else
        printf("xyz is the %dth\n",target->pvValue);
    Link_print(head);
    return ;
}

static void test_Link_remove(head)
{
    printf("\ntest the Link_remove---------------\n");
    Link_t thead =head;
    errno = 0;
    Link_t target = Link_remove(head,"xixi");
    test_try(errno != -1,"The func remove()");
//    printf("runing!\n");
    if(target == NULL)
        printf("xixi not exist in the link!\n");
    else
        printf("xixi is the %dth\n",target->pvValue);
    errno = 0;
    target = Link_remove(head,"xyz");
    test_try(errno != -1,"The func remove()");
    if(target == NULL)
        printf("xyz not exist in the link!\n");
    else
        printf("xyz is the %dth\n",target->pvValue);
    Link_print(thead);
    return ;
}

static void test_all()
{
  test_begin();
  test_Link_new_free();
  Link_t head = Link_new();
  test_Link_put(head);
  test_Link_getLength(head);
  test_Link_contains(head);
  test_Link_get(head);
  test_Link_replace(head);
  test_Link_remove(head);
  test_end(head);
}

int main() {
  test_all();
  return 0;
}


