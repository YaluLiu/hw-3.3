LinkList - A Struct Link List
======================================================
It's a LinkList.I will code it ,organize my code by
the git commands.

File List
=================
LinkList.h  - It is the header file of this library.
LinkList.c  - It contains the c file of LinkList.h. It 
    implements all of functions delcare in LinkList.h
    M1UnitTest.c- It is the unit test file of LinkList.
    Makefile    - The makefile of this library.

    interface
    =================
    // Create a new list, if there's not enough memory, 
    // return NULL; if success, return the pointer.
    Link_t LinkTable_new(void);
    
    // Free the link which oLink point to, free oLink's
    // used memory.
    void LinkTable_free(Link_t oLink);
    
    //Return nodes number in oLink.
    int LinkTable_getLength(Link_t oLink);
    
    // If pcKey not exists in oLink, insert pcKey and 
    // pvValue and return 1, if there's not enough memory 
    // OR pvValue is already in oLink, return 0.
    int Link_put(Link_t oLink, const char *pcKey, 
                                            const void *pvValue);

// Find pcKey in oLink, if found then return node's 
// value, if cannot find, return NULL. Notice that 
// this function won't modify oLink.
void *Link_get(Link_t oLink, const char *pcKey);

// Find pcKey in oLink, if found then replace node's
// value to pvValue, return pointer to node's old value,
// if cannot find, return NULL.
void *Link_replace(Link_t oLink, const char *pcKey,
                                        const void *pvValue);
// Find pcKey in oLink, if found, return 1, else return 0.
// Notice that this function won't modify oLink.
int Link_constains(Link_t oLink, const char *pcKey);

// Find pcKey in oLink, if found, remove  it's value, then
// return a pointer to it's old value; else return NULL pointer.
void *Link_remove(Link_t oLink, const char *pcKey);

How to make
=================
Use command:
$ make
$ make run

Author
=================
Yalu

Thanks!
=================
